---
title: "Prosperitní přístup"
author: "Lukáš Rejnek"
site: bookdown::bookdown_site
documentclass: book
output:
  bookdown::gitbook:
    includes:
      in_header: head.html
  #bookdown::pdf_book: default
---

# Informace o projektu
**Prosperitní přístup** je myšlenkový směr nové generace. Jeho cílem je pomoci lidem popsaním vhodného cíle pro stát, ve kterém žijí. Jedná o komunitní projekt a tak má každý možnost se k myšlenkám vyjádřit a navrhnout úpravy.

## Důvod vzniku
Na internetu existuje mnoho kvalitních komentářů ke společenskému dění. Jejich autoři často kritizují chování vlády a přicházejí s nápady na vylepšení (a ušetření :). Často si ale nevšímají různorodých zájmů ostaních občanů. Národní hrdost je pro někoho (na první pohled) důležitější, než lépe placená práce. Komentátor pak argumentuje pro činnost, kterou mnoho lidí zatím ani nechce.

Tento problém řeší Prosperitní přístup, který zmenšuje různorodost zájmů a tak umožňuje efektivněji argumentovat a lépe vládnout.

## Komentáře a úpravy
Komentovat části textu můžete pomocí [Hypothesis](https://web.hypothes.is/). Otevřete ji kliknutím na šipku v pravém horním rohu nebo označením části textu.

Knížka je napsaná pomocí formátovacího jazyka [RMarkdown](https://rmarkdown.rstudio.com/) a uložená v [GitLab](https://gitlab.com/rejnek/prosperita) repozitáři. Pro úpravu otevřete repozitář a přepište obsah odpovídajícho souboru. Pro zveřejnění je pak potřeba jenom souhlas správce projektu.
