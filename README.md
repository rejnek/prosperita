# Prosperitní přístup
Prosperitní přístup ([https://rejnek.gitlab.io/prosperita/](https://rejnek.gitlab.io/prosperita/)) je myšlenkový směr, který popsisuje cíl lidské společnosti a států, do kterých se lidská společnost organizuje. Jedná o komunitní projekt a tak má každý možnost se k myšlenkám vyjádřit a navrhnout úpravy.

## Důvod vzniku
Na internetu existuje mnoho kvalitních pravicových komentářů ke společenskému dění. Jejich autoři často kritizují chování vlády a přicházejí s nápady na vylepšení (a ušetření :). Často si ale nevšímají různorodých zájmů většiny osob. Národní hrdost je pro někoho (na první pohled) důležitější, než lépe placená práce. Komentátor pak argumentuje pro činnost, kterou mnoho lidí zatím ani nechce.

Tento problém řeší Prosperitní přístup, který zmenšuje různorodost zájmů a tak umožňuje efektivněji argumentovat a lépe vládnout.

## Úpravy
Vzhledem k malé velikosti projektu je zatím vhodné mě předem kontaktovat na email luky[zavinac]rejnek.cz a domluvit se.
Knížka je napsaná pomocí formátovacího jazyka [RMarkdown](https://rmarkdown.rstudio.com/) a pro menší úpravy stačí otevřít odpovídající soubor a přepsat jeho obsah. Pro zveřejnění je pak potřeba jenom můj souhlas.

Pro dropnější postřehy je možné označit část textu webové knížky pomocí [Hypothesis](https://web.hypothes.is/) a napsat komentář. Přistoupit k ní můžete i kliknutím na šipku v pravém horním rohu knížky.
